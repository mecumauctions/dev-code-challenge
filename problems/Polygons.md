# Fledgling Mathematician

I am a fledgling mathematician who cannot seem to grasp geometry to save my life.  Instead of listening
to my many geometry teachers when discussing 2D and 3D polygons, I was writing games on my calculator.
Please help me!

I am only working with the following polygons:

	* Circle
	* Triangle
	* Square
	* Sphere
	* Regular Tetrahedron
	* Cube

## Requirements

As a fledgling mathematician, I need to be able to instantiate new polygons and set or get the following information about each polygon:

1. Number of Sides/Edges
2. Number of Vertices
3. Number of Faces *might only be applicable to some shapes*
4. Any other information to help me calculate the area and/or volume for a given polgyon (think radius, length, width, etc.)

As a fledgling mathematician, I need to be able to calculate the area/surface area for any polygon I create.  The output of this function needs to be a decimal value.

As a fledgling mathematician, I need to be able to calculate the volume of any 3D polygon I create.  The output of this function needs to be a decimal value.

## Output

I need to know the following:

1. Area of a circle with radius of 5 cm
2. Area of a triangle with height of 10 cm and a base of 6 cm
3. Area of a square with sides of 6 cm
4. Surface area and volume of a sphere with radius 5 cm
5. Surface area and volume of a tetrahedron with a side of 7 cm
6. Surface area and volume of a cube with sides of 8 cm.

## Hints/Cheat Sheets

[2D formulas](http://www.math-salamanders.com/image-files/geometry-terms-and-definitions-geometry-cheat-sheet-4-2d-shapes-formulas.gif)

[3D formulas](http://www.math-salamanders.com/image-files/high-school-geometry-help-geometry-cheat-sheet-5-3d-shape-formulas.gif)

You can use M_PI or pi() for getting the value of PI in PHP.
