# Mecum Auctions Dev Challenges

This repository contains programming challenges for Mecum Auctions developer applications

## Problems

1. [FizzBuzzBang](problems/FizzBuzzBang.md)
2. [Polygons](problems/Polygons.md)
3. [Selectors](problems/Selectors.md)

## License and Authors

Author:: Cade Cannon (<ccannon@mecum.com>)
